
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var sessionId = getSessionId();
  var ehrId;
  var ime;
  var priimek;
  var spol;
  var datumRojstva;
  var datumUraMeritve;
  var sisTlak;
  var disTlak; 
  
  switch(stPacienta) {
  	case 1:	ime = "Tinka";
  			priimek = "Nizka";
  			datumRojstva = "1990-05-11";
  			spol = "FEMALE";
  			sisTlak = [79,89,91,86,94,102,85,89,93,83];
  			disTlak = [58,59,55,61,58,61,59,56,58,56] ;
  			datumUraMeritve = ["1990-05-11T02:20","1995-01-10T10:45","1995-04-02T06:15","1999-09-20T13:44","2005-11-02T09:02","2006-03-19T17:53","2011-10-25T07:34","2015-11-18T18:02","2016-05-06T08:14","2018-03-16T01:52"];
  			break;
  	case 2:	ime = "Stane";
  			priimek = "Normalko";
  			datumRojstva = "1998-10-02";
  			spol = "MALE";
  			sisTlak = [96,101,112,125,102,135,109,104,122,118];
  			disTlak = [61,68,72,84,75,89,73,68,83,76];
  			datumUraMeritve = ["1998-10-03T00:05","1999-03-01T06:10","2002-01-21T14:03","2004-09-30T09:16","2007-12-01T18:46","2008-01-06T07:04","2012-03-21T01:55","2016-09-22T10:19","2016-10-02T09:05","2018-01-30T16:42"];
  			break;
  	case 3: ime = "Tone";
  			priimek = "Visok";
  			datumRojstva = "1997-04-26";
  			spol = "MALE";
  			sisTlak = [109,108,123,153,159,161,155,166,148,145];
  			disTlak = [82,81,84,91,104,91,102,106,87,86];
  			datumUraMeritve = ["1997-04-26T16:40","1999-05-13T16:34","2001-10-26T22:05","2005-03-22T06:29","2005-04-02T07:06","2006-11-06T10:46","2007-01-19T17:11","2012-09-02T08:42","2012-10-20T09:01","2018-04-06T07:56"];
  			break;
  	default: console.log("Neveljavna številka pacienta!");
  			 return "";
  }
	
  	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            gender: spol,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		            	for(var i = 0; i < 10; i++) {
							dodajMeritevGenerirano(sessionId,ehrId,datumUraMeritve[i],sisTlak[i],disTlak[i]);
						}
						$("#seznamPacientov1").append("<option value=" + ehrId + ">" + ime + " " + priimek + "</option>");
						$("#seznamPacientov2").append("<option value=" + ehrId + ">" + ime + " " + priimek + "</option>");
		            }
		        });
		    }
		});
 
  return ehrId;
}

/**
 * Funkcija za dodajanje meritev generiranim pacientom
 */
function dodajMeritevGenerirano(sessionId,ehrId,datumInUra,sistolicniKrvniTlak,diastolicniKrvniTlak) {
	
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		       
		    },
		    
		});
}

/**
 * Funkcija za generiranje vseh testnih pacientov
 */
function generirajPaciente() {
	clearBox('seznamPacientov1');
	clearBox('seznamPacientov2');
	$("#seznamPacientov1").append("<option disabled selected value> -- izberite pacienta -- </option>");
	$("#seznamPacientov2").append("<option disabled selected value> -- izberite pacienta -- </option>");
	
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
	alert("Vzorčni pacienti so uspešno kreirani!");
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#novIme").val();
	var priimek = $("#novPriimek").val();
    var datumRojstva = $("#novDatum").val() + "T00:00:00.000Z";
    var spol = $("#novSpol").val();
    
    if(spol == "m") {
    	spol = "MALE";
    }
    spol = "FEMALE";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            gender: spol,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    //$("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura, sistolični
 * in diastolični krvni tlak.
 */
function dodajMeritev() {
	sessionId = getSessionId();

	var ehrId = $("#ehrID1").val();
	var datumInUra = $("#datumUra").val();
	
	var sistolicniKrvniTlak = $("#skt").val();
	var diastolicniKrvniTlak = $("#dkt").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#meritveSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#meritveSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#meritveSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

/**
 * Funkcija za zapiranje pregleda
 */
function pocisti() {
	$("#prikaz").hide();
	$("#pocistiGumb").hide();
	$("#graf").hide();
	$("#ugotovitev").hide();
	$("#nearby").hide();
	$("#map").hide();
	clearBox('meritve');
	clearBox('ugotovitev');
	document.getElementById("pokaziGumb").disabled = false;
}

/**
 * Funkcija za ciscenje html elementov
 */
function clearBox(elementID) {
    document.getElementById(elementID).innerHTML = "";
}

var tabTlakov;

/**
 * Funkcija za branje meritev pacienta
 */
function preberi(ehrId,callback) {
	
	$("#prikaz").show();
    document.getElementById("pokaziGumb").disabled = true;
    $("#pocistiGumb").show();
    $("#graf").show();
    $("#ugotovitev").show();
    $("#map").show();

	var sis;
	var dis;
	var dateTime;
	var datum;
	var dodaj = "";

	var sessionId = getSessionId();
	tabTlakov = [];


	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var bolnik = data.party;
				$("#osebniPodatki").html("<span> Ime in priimek: <strong>" + bolnik.firstNames + " " + bolnik.lastNames + "</strong></span>");
    			$.ajax({ 
    				url: baseUrl + "/view/" + ehrId + "/blood_pressure",
    				type: "GET",
    				headers: {"Ehr-Session" : sessionId},
    				success: function(data) {
    					if(data.length > 0) {
    						for(var i in data) {
    							sis = data[i].systolic;
    							dis = data[i].diastolic;
    							dateTime = data[i].time;
    							datum = new Date(dateTime);
    							dodaj = '<div class="panel panel-primary">\
    							<div class="panel-heading">\
              					<h4 class="panel-title">\
               					<a data-toggle="collapse" data-parent="#parent" href="#collapse' + (parseInt(i)+1) +'">\
              					Meritev ' + (parseInt(i) + 1) + '</a></h4>\
            					</div><div id="collapse' + (parseInt(i)+1) + '" class="panel-collapse collapse">\
              					<div class="panel-body"><strong>Sistolični (zgornji) krvni tlak: </strong>' + sis + 
              					'</br><strong>Diastolični (spodnji) krvni tlak: </strong>' + dis + '</br><strong>Datum in čas meritve: </strong>' + datum +'</div></div></div>';
              					$("#meritve").append(dodaj);
    							dodajTlak(sis, dis);
    						}
    						kaksenTlak();
    						drawChart();
    						callback(data);
    					}
    				}
    			});
			},
		});
	}
}

/**
 * Funkcija za dodajanje vrednosti tlaka v tabelo
 */
function dodajTlak(sis, dis){
	tabTlakov.push([dis, sis]);
}

/**
 * Funkcija za ugotavljanje diagnoze
 */
function kaksenTlak() {
	var sis = 0;
	var dis = 0;
	var stMeritev = 0;
	
	for(var i = 0; i < tabTlakov.length; i++) {
		sis += tabTlakov[i][1];	
		dis += tabTlakov[i][0];
		stMeritev++;
	}
	
	// racunanje povprecja
	sis /= stMeritev;
	dis /= stMeritev;
	
	
	// nizek krvni tlak
	if(sis < 91 || dis < 61) {
			$("#ugotovitev").append("<p>Ugotavljamo, da je vaš povprečni krvni tlak <strong>prenizek</strong>, zato vam svetujemo obisk osebnega zdravnika. V kolior to ni mogoče so na zemljevidu označene najbljižje bolnišnice (za prikaz morate v svojem brskalniku dovoliti uporabo lokacije).</p>");
			$("#nearby").show();
			pokaziZemljevid();
	  // normalen krvni tlak	
	} else if((sis > 90 && sis < 140) && (dis > 60 && dis < 90)) {
			$("#ugotovitev").append("<p>Ugotavljamo, da je vaš povprečni krvni tlak <strong>normalen</strong>. Priporočamo vam, da kljub temu živite zdravo in poskrbite za zadostno količino gibanja.</p>");
	  // visok krvni tlak	
	} else if(sis > 139 || dis > 89) {
			$("#ugotovitev").append("<p>Ugotavljamo, da je vaš povprečni krvni tlak <strong>previsok</strong>, zato vam svetujemo obisk osebnega zdravnika. V kolior to ni mogoče so na zemljevidu označene najbljižje bolnišnice (za prikaz morate v svojem brskalniku dovoliti uporabo lokacije).</p>");
			$("#nearby").show();
			pokaziZemljevid();
	}
}

/**
 * Funkcija za risanje grafa
 */
function drawChart(podatki) {

    var data = new google.visualization.DataTable;
   
	
  	data.addColumn('number');
    data.addColumn('number');

    
    data.addRows(tabTlakov);
    var options = {
        title: '',
        hAxis: {title: 'Diastolični tlak [mm Hg]', minValue: 0, maxValue: 140, gridlines: {count: 15, color: 'none'}},
        vAxis: {title: 'Sistolični tlak [mm Hg]', minValue: 0, maxValue: 200, gridlines: {count: 21, color: 'none'}},
		legend: 'none',
		colors: ['FFFFFF'],
		backgroundColor: 'none'
    };

    var chart = new google.visualization.ScatterChart(document.getElementById('graf'));
    chart.draw(data, options);

}

/**
 * Funkcija za risanje zemljevida
 */
function pokaziZemljevid(){
        var map;
        var infoWindow;

        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 10
        });

        if (navigator.geolocation) {
            
          navigator.geolocation.getCurrentPosition(function(position) {
              
            var pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
            infoWindow = new google.maps.InfoWindow({map: map});
            infoWindow.setPosition(pos);
            infoWindow.setContent('Vaša lokacija.');
            map.setCenter(pos);
            
            var service = new google.maps.places.PlacesService(map);
            service.nearbySearch({
                location: pos,
                radius: 50000,
                type: ['hospital']
            }, callback);
            
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
          
        } else {
          handleLocationError(false, infoWindow, map.getCenter());
        }

      function callback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i], "");
          }
        }
      }

      function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function() {
          infoWindow.setContent(place.name);
          infoWindow.open(map, this);
        });
      }
      
      
      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
      }
    }
      
      
$(document).ready(function() {
	$("#pokaziGumb").click(function(){
		google.charts.load('current', {packages: ['corechart']});
		
	});	

	$("#pokaziGumb").click(function(){
		var ehrid = $("#ehrID2").val();
		if(ehrid == null || ehrid.length == 0){
			$("#ehrID2").attr("placeholder", "Prosim vnesite podatke!");

		}
		else
			preberi(ehrid, function(data){
		});
	});	
	
	$('#seznamPacientov1').change(function() {
		$("#ehrID1").val($(this).val());
	});
	
	$('#seznamPacientov2').change(function() {
		$("#ehrID2").val($(this).val());
	});
});